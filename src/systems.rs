use amethyst::{
  animation::{AnimationCommand, AnimationControl, AnimationControlSet, ControlState, EndControl},
  core::nalgebra::{Real, Vector3},
  ecs::System,
  renderer::SpriteRender,
};
use crate::components::{Animations, Directions};
use crate::states::loading::AnimationResource;

pub mod player_system;
pub mod zombie_system;
pub use self::player_system::PlayerSystem;
pub use self::zombie_system::ZombieSystem;

pub trait AnimatedSystem<'a>: System<'a> {
  fn animation(&self) -> Animations;

  fn set_animation(&mut self, anim: Animations);

  fn switch_animation(
    &mut self,
    new: Animations,
    anim_res: &AnimationResource,
    anim_ctl: &mut AnimationControlSet<Animations, SpriteRender>,
  ) {
    anim_ctl.abort(self.animation());
    let new_anim = anim_res.get(&new).unwrap().clone();
    let new_anim_ctl = AnimationControl::new(
      new_anim.clone(),
      EndControl::Loop(None),
      ControlState::Requested,
      AnimationCommand::Start,
      10.,
    );
    anim_ctl.insert(new, new_anim_ctl);
    self.set_animation(new);
  }

  fn get_animation(move_dir: Vector3<f32>) -> Animations {
    if move_dir.norm() < 0.01 {
      Animations::Wait
    } else {
      let ang = move_dir.y.atan2(move_dir.x);
      if ang > 0. {
        if ang < f32::frac_pi_4() {
          Animations::Walk(Directions::E)
        } else if ang > 3. * f32::frac_pi_4() {
          Animations::Walk(Directions::W)
        } else {
          Animations::Walk(Directions::N)
        }
      } else {
        if ang < -3. * f32::frac_pi_4() {
          Animations::Walk(Directions::W)
        } else if ang > -f32::frac_pi_4() {
          Animations::Walk(Directions::E)
        } else {
          Animations::Walk(Directions::S)
        }
      }
    }
  }

  fn mouse_to_screen(mouse: (f64, f64), dimensions: (f64, f64)) -> Vector3<f32> {
    Vector3::new(
      (mouse.0 / dimensions.0 * 2.0 - 1.0) as f32,
      (-mouse.1 / dimensions.1 * 2.0 + 1.0) as f32,
      0.,
    )
  }
}
