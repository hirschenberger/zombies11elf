use amethyst::{
    animation::AnimationBundle,
    core::{frame_limiter::FrameRateLimitStrategy, transform::TransformBundle},
    input::InputBundle,
    prelude::*,
    renderer::{
        ColorMask, DepthMode, DisplayConfig, DrawFlat2D, Pipeline, RenderBundle, SpriteRender,
        Stage, ALPHA,
    },
    ui::{DrawUi, UiBundle},
    utils::{application_dir, application_root_dir, fps_counter::FPSCounterBundle},
    Logger,
};
use std::time::Duration;

mod bundles;
mod components;
mod states;
mod systems;

use crate::{bundles::GameBundle, components::Animations, states::Loading};

fn main() -> amethyst::Result<()> {
    let logger = Logger::from_config(Default::default());
    logger
        .level_for("gfx_device_gl", log::LevelFilter::Warn)
        .start();

    let config = DisplayConfig::load(application_dir("resources/display_config.ron")?);

    let pipe = Pipeline::build().with_stage(
        Stage::with_backbuffer()
            .clear_target([0.00196, 0.23726, 0.21765, 1.0], 1.0)
            .with_pass(DrawFlat2D::new().with_transparency(
                ColorMask::all(),
                ALPHA,
                Some(DepthMode::LessEqualWrite), // Tells the pipeline to respect sprite z-depth
            ))
            .with_pass(DrawUi::new()),
    );

    let game_data = GameDataBuilder::default()
        .with_bundle(InputBundle::<String, String>::new())?
        .with_bundle(GameBundle)?
        .with_bundle(
            RenderBundle::new(pipe, Some(config))
                .with_sprite_sheet_processor()
                .with_sprite_visibility_sorting(&[]),
        )?
        .with_bundle(AnimationBundle::<Animations, SpriteRender>::new(
            "player_animation",
            "player_sampler",
        ))?
        .with_bundle(TransformBundle::new().with_dep(&["player_system"]))?
        .with_bundle(FPSCounterBundle::default())?
        .with_bundle(UiBundle::<String, String>::new())?;

    let resource_path = application_root_dir()?.join("resources");
    let mut game = Application::build(resource_path, Loading::default())?
        .with_frame_limit(
            FrameRateLimitStrategy::SleepAndYield(Duration::from_millis(2)),
            144,
        )
        .build(game_data)?;

    game.run();

    Ok(())
}
