use amethyst::ecs::{Component, DenseVecStorage};

#[derive(PartialEq, Eq, Debug, Hash, Clone, Copy)]
pub enum Animations {
    Walk(Directions),
    Wait,
}

#[derive(PartialEq, Eq, Debug, Hash, Clone, Copy)]
pub enum Directions {
    N,
    E,
    S,
    W,
}

pub struct Player;

impl Component for Player {
    type Storage = DenseVecStorage<Self>;
}

pub struct Zombie;

impl Component for Zombie {
    type Storage = DenseVecStorage<Self>;
}
