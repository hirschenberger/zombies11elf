use amethyst::{
    core::bundle::{Result, SystemBundle},
    ecs::prelude::DispatcherBuilder,
    utils::ortho_camera::CameraOrthoSystem,
};

use crate::systems::{PlayerSystem, ZombieSystem};

pub struct GameBundle;

impl<'a, 'b> SystemBundle<'a, 'b> for GameBundle {
    fn build(self, builder: &mut DispatcherBuilder<'a, 'b>) -> Result<()> {
        builder.add(PlayerSystem::default(), "player_system", &["input_system"]);
        builder.add(ZombieSystem::default(), "zombie_system", &["input_system"]);
        builder.add(CameraOrthoSystem::default(), "camera_ortho_system", &[]);
        Ok(())
    }
}
