use amethyst::{
  animation::AnimationControlSet,
  core::nalgebra::Unit,
  core::{timing::Time, Transform},
  ecs::prelude::{Read, ReadExpect, ReadStorage, System, WriteStorage, *},
  renderer::{ScreenDimensions, SpriteRender},
};

use crate::components::{Animations, Player, Zombie};
use crate::states::loading::AnimationResource;
use crate::systems::AnimatedSystem;
//use log::warn;

pub struct ZombieSystem {
  animation: Animations,
}

impl<'a> AnimatedSystem<'a> for ZombieSystem {
  fn animation(&self) -> Animations {
    self.animation
  }

  fn set_animation(&mut self, anim: Animations) {
    self.animation = anim;
  }
}

impl Default for ZombieSystem {
  fn default() -> Self {
    ZombieSystem {
      animation: Animations::Wait,
    }
  }
}

impl<'a> System<'a> for ZombieSystem {
  type SystemData = (
    ReadStorage<'a, Zombie>,
    ReadStorage<'a, Player>,
    WriteStorage<'a, Transform>,
    WriteStorage<'a, AnimationControlSet<Animations, SpriteRender>>,
    ReadExpect<'a, ScreenDimensions>,
    ReadExpect<'a, AnimationResource>,
    Read<'a, Time>,
  );

  fn run(
    &mut self,
    (zombie, player, mut trans, mut anim, _screen, anim_res, time): Self::SystemData,
  ) {
    if let Some((_player, player_trans)) = (&player, &trans).join().next() {
      let player_pos = player_trans.translation().clone();

      for (_zombie, trans, anim) in (&zombie, &mut trans, &mut anim).join() {
        let move_dir = player_pos - trans.translation();
        let new_anim = Self::get_animation(move_dir);

        if new_anim != Animations::Wait {
          trans.move_along_global(Unit::new_normalize(move_dir), 0.5 * time.delta_seconds());
        }

        if new_anim != self.animation {
          self.switch_animation(new_anim, &*anim_res, anim);
        }
      }
    }
  }
}
