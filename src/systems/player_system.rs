use amethyst::{
  animation::AnimationControlSet,
  core::nalgebra::Unit,
  core::{timing::Time, Transform},
  ecs::prelude::{Read, ReadExpect, ReadStorage, System, WriteStorage, *},
  input::InputHandler,
  renderer::{ScreenDimensions, SpriteRender},
};

use crate::components::{Animations, Player};
use crate::states::loading::AnimationResource;
use crate::systems::AnimatedSystem;
//use log::warn;

pub struct PlayerSystem {
  animation: Animations,
}

impl<'a> AnimatedSystem<'a> for PlayerSystem {
  fn animation(&self) -> Animations {
    self.animation
  }

  fn set_animation(&mut self, anim: Animations) {
    self.animation = anim;
  }
}

impl Default for PlayerSystem {
  fn default() -> Self {
    PlayerSystem {
      animation: Animations::Wait,
    }
  }
}

impl<'a> System<'a> for PlayerSystem {
  type SystemData = (
    ReadStorage<'a, Player>,
    WriteStorage<'a, Transform>,
    WriteStorage<'a, AnimationControlSet<Animations, SpriteRender>>,
    ReadExpect<'a, ScreenDimensions>,
    ReadExpect<'a, AnimationResource>,
    Read<'a, Time>,
    Read<'a, InputHandler<String, String>>,
  );

  fn run(
    &mut self,
    (player, mut trans, mut anim, screen, anim_res, time, input): Self::SystemData,
  ) {
    if let Some(m) = input.mouse_position() {
      let mouse_pos = Self::mouse_to_screen(m, (screen.width() as f64, screen.height() as f64));
      for (_player, trans, anim) in (&player, &mut trans, &mut anim).join() {
        let move_dir = mouse_pos - trans.translation();
        let new_anim = Self::get_animation(move_dir);

        if new_anim != Animations::Wait {
          trans.move_along_global(Unit::new_normalize(move_dir), time.delta_seconds());
        }

        if new_anim != self.animation {
          self.switch_animation(new_anim, &*anim_res, anim);
        }
      }
    }
  }
}
