use amethyst::{
  animation::{
    Animation, InterpolationFunction, Sampler, SpriteRenderChannel, SpriteRenderPrimitive,
  },
  assets::{AssetStorage, Completion, Handle, Loader, ProgressCounter},
  prelude::*,
  renderer::{
    JpgFormat, PngFormat, Sprite, SpriteRender, SpriteSheet, SpriteSheetHandle, Texture,
    TextureCoordinates, TextureHandle, TextureMetadata,
  },
  ui::{UiCreator, UiFinder, UiText},
};

use crate::{
  components::{Animations, Directions},
  states::Game,
};
use a_range;
use log::{error, warn};
use std::collections::HashMap;
use std::ffi::OsStr;
use std::path::Path;

#[derive(Debug)]
struct SpriteSheetDefinition {
  /// Width of each individual sprite on the sprite sheet.
  pub sprite_w: f32,
  /// Height of each individual sprite on the sprite sheet.
  pub sprite_h: f32,
  /// Number of rows in the sprite sheet.
  ///
  /// This is the number of sprites counting down the sheet.
  pub row_count: usize,
  /// Number of columns in the sprite sheet.
  ///
  /// This is the number of sprites counting across the sheet.
  pub column_count: usize,
  /// Whether or not there is a 1 pixel border between sprites.
  pub has_border: bool,
}

pub type SpriteSheetResource = HashMap<String, SpriteSheetHandle>;
pub type TextureResource = HashMap<String, TextureHandle>;
pub type AnimationHandle = Handle<Animation<SpriteRender>>;
pub type AnimationResource = HashMap<Animations, AnimationHandle>;

impl SpriteSheetDefinition {
  /// Returns a new sprite sheet definition.
  ///
  /// # Parameters:
  ///
  /// * `sprite_w`: Width of each individual sprite on the sprite sheet.
  /// * `sprite_h`: Height of each individual sprite on the sprite sheet.
  /// * `row_count`: Number of rows in the sprite sheet.
  /// * `column_count`: Number of columns in the sprite sheet.
  /// * `has_border`: Whether or not there is a 1 pixel border between sprites.
  fn new(
    sprite_w: f32,
    sprite_h: f32,
    row_count: usize,
    column_count: usize,
    has_border: bool,
  ) -> Self {
    SpriteSheetDefinition {
      sprite_w,
      sprite_h,
      row_count,
      column_count,
      has_border,
    }
  }
}

#[derive(Default)]
pub struct Loading {
  progress: ProgressCounter,
}

impl SimpleState for Loading {
  fn on_start(&mut self, data: StateData<GameData>) {
    data.world.exec(|mut creator: UiCreator| {
      creator.create("fps.ron", &mut self.progress);
      creator.create("loading.ron", &mut self.progress);
    });

    let mut sprite_sheets = SpriteSheetResource::new();

    let player_tx = load_texture(data.world, "assets/sprites/player.png", &mut self.progress);
    let zombie_tx = load_texture(data.world, "assets/sprites/zombie.png", &mut self.progress);
    let player_def = SpriteSheetDefinition::new(64., 64., 21, 13, false);
    let player_sprite = load_spritesheet(data.world, player_tx, &player_def, &mut self.progress);
    let zombie_sprite = load_spritesheet(data.world, zombie_tx, &player_def, &mut self.progress);
    sprite_sheets.insert("player".to_string(), player_sprite);
    sprite_sheets.insert("zombie".to_string(), zombie_sprite);

    let mut textures = TextureResource::new();
    let bg_tx = load_texture(
      data.world,
      "assets/backgrounds/tiles.jpg",
      &mut self.progress,
    );
    textures.insert("background".to_string(), bg_tx);

    let mut animations = AnimationResource::new();
    add_animation(
      data.world,
      &mut animations,
      Animations::Wait,
      &a_range::from(27).up_to(33).to_vec(),
      &mut self.progress,
    );

    add_animation(
      data.world,
      &mut animations,
      Animations::Walk(Directions::N),
      &a_range::from(105).up_to(113).to_vec(),
      &mut self.progress,
    );

    add_animation(
      data.world,
      &mut animations,
      Animations::Walk(Directions::W),
      &a_range::from(118).up_to(126).to_vec(),
      &mut self.progress,
    );

    add_animation(
      data.world,
      &mut animations,
      Animations::Walk(Directions::S),
      &a_range::from(131).up_to(139).to_vec(),
      &mut self.progress,
    );

    add_animation(
      data.world,
      &mut animations,
      Animations::Walk(Directions::E),
      &a_range::from(144).up_to(152).to_vec(),
      &mut self.progress,
    );

    data.world.add_resource(sprite_sheets);
    data.world.add_resource(textures);
    data.world.add_resource(animations);
  }

  fn update(&mut self, data: &mut StateData<GameData>) -> SimpleTrans {
    match self.progress.complete() {
      Completion::Failed => {
        error!("Failed loading assets: {:?}", self.progress.errors());
        Trans::Quit
      }
      Completion::Complete => {
        warn!("Assets loaded, swapping state");
        if let Some(entity) = data.world.exec(|finder: UiFinder| finder.find("loading")) {
          let _ = data.world.delete_entity(entity);
        };
        Trans::Switch(Box::new(Game::new()))
      }
      Completion::Loading => {
        let progress =
          self.progress.num_finished() as f32 / self.progress.num_assets() as f32 * 100.;
        if let Some(entity) = data.world.exec(|finder: UiFinder| finder.find("loading")) {
          let mut ui_text = data.world.write_storage::<UiText>();
          if let Some(loading_text) = ui_text.get_mut(entity) {
            loading_text.text = format!("Loading... {:.1}%", progress);
          };
        };
        Trans::None
      }
    }
  }
}

fn add_animation(
  world: &mut World,
  animations: &mut AnimationResource,
  id: Animations,
  idxs: &[usize],
  progress: &mut ProgressCounter,
) {
  let sprite_indices: Vec<_> = idxs
    .iter()
    .map(|&n| SpriteRenderPrimitive::SpriteIndex(n))
    .collect();

  let keyframes: Vec<_> = (0u16..sprite_indices.len() as u16).map(f32::from).collect();
  let sprite_idx_sampler = Sampler {
    input: keyframes,
    function: InterpolationFunction::Step,
    output: sprite_indices,
  };

  let loader = world.write_resource::<Loader>();

  let animation = {
    let anim_handle = loader.load_from_data(sprite_idx_sampler, (), &world.read_resource());
    Animation::<SpriteRender>::new_single(0, SpriteRenderChannel::SpriteIndex, anim_handle)
  };
  animations.insert(
    id,
    loader.load_from_data(animation, progress, &world.read_resource()),
  );
}

fn load_texture(
  world: &mut World,
  image_path: &str,
  progress: &mut ProgressCounter,
) -> TextureHandle {
  let loader = world.read_resource::<Loader>();
  let tx_storage = world.read_resource::<AssetStorage<Texture>>();
  let extension = Path::new(image_path).extension();

  match extension.and_then(OsStr::to_str) {
    Some("png") => loader.load(
      image_path,
      PngFormat,
      TextureMetadata::srgb_scale(),
      progress,
      &tx_storage,
    ),
    Some("jpg") => loader.load(
      image_path,
      JpgFormat,
      TextureMetadata::srgb_scale(),
      progress,
      &tx_storage,
    ),
    _ => panic!("Unknown sprite format"),
  }
}

fn load_spritesheet(
  world: &mut World,
  texture: TextureHandle,
  definition: &SpriteSheetDefinition,
  progress: &mut ProgressCounter,
) -> SpriteSheetHandle {
  let mut sprites = Vec::with_capacity(definition.row_count * definition.column_count);
  let (offset_w, offset_h) = offset_distances(&definition);
  let (image_w, image_h) = (
    offset_w * definition.column_count as f32,
    offset_h * definition.row_count as f32,
  );

  for row in 0..definition.row_count {
    for col in 0..definition.column_count {
      // Sprites are numbered in the following pattern:
      //
      //  0  1  2  3  4
      //  5  6  7  8  9
      // 10 11 12 13 14
      // 15 16 17 18 19

      let offset_x = offset_w * col as f32;
      let offset_y = offset_h * row as f32;
      let sprite = create_sprite(
        image_w,
        image_h,
        definition.sprite_w,
        definition.sprite_h,
        offset_x,
        offset_y,
      );

      sprites.push(sprite);
    }
  }
  let loader = world.read_resource::<Loader>();
  loader.load_from_data(
    SpriteSheet { texture, sprites },
    progress,
    &world.read_resource::<AssetStorage<SpriteSheet>>(),
  )
}

fn offset_distances(definition: &SpriteSheetDefinition) -> (f32, f32) {
  if definition.has_border {
    (definition.sprite_w + 1., definition.sprite_h + 1.)
  } else {
    (definition.sprite_w, definition.sprite_h)
  }
}

fn create_sprite(
  image_w: f32,
  image_h: f32,
  sprite_w: f32,
  sprite_h: f32,
  pixel_left: f32,
  pixel_top: f32,
) -> Sprite {
  let pixel_right = pixel_left + sprite_w;
  let pixel_bottom = pixel_top + sprite_h;

  // Texture coordinates are expressed as fractions of the position on the image.
  let left = pixel_left / image_w;
  let right = pixel_right / image_w;
  let top = 1.0 - pixel_top / image_h;
  let bottom = 1.0 - pixel_bottom / image_h;

  let tex_coords = TextureCoordinates {
    left,
    right,
    bottom,
    top,
  };

  Sprite {
    width: sprite_w,
    height: sprite_h,
    offsets: [0., 0.], //[sprite_w / 2.0, sprite_h / 2.0],
    tex_coords,
  }
}
