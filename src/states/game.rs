use amethyst::{
  animation::{get_animation_set, AnimationCommand, AnimationControl, ControlState, EndControl},
  core::{timing::Time, transform::Transform},
  ecs::prelude::Entity,
  input::{is_close_requested, is_key_down},
  prelude::*,
  renderer::{Camera, SpriteRender, Transparent},
  ui::{UiCreator, UiFinder, UiText},
  utils::{
    fps_counter::FPSCounter,
    ortho_camera::{CameraNormalizeMode, CameraOrtho},
  },
  winit::VirtualKeyCode,
};

use crate::components::{Animations, Player, Zombie};
use crate::states::loading::{AnimationResource, SpriteSheetResource, TextureResource};

//use log::warn;

pub struct Game {
  fps_display: Option<Entity>,
}

impl Game {
  pub fn new() -> Self {
    Game { fps_display: None }
  }
}

impl SimpleState for Game {
  fn on_start(&mut self, data: StateData<GameData>) {
    let StateData { world, .. } = data;
    world.exec(|mut creator: UiCreator| {
      creator.create("fps.ron", ());
    });
    init_background(world);
    init_player(world);
    init_zombies(world);
    init_camera(world);
  }

  fn update(&mut self, state: &mut StateData<GameData>) -> SimpleTrans {
    // create fps display if it does not exist
    if self.fps_display.is_none() {
      state.world.exec(|finder: UiFinder| {
        if let Some(entity) = finder.find("fps") {
          self.fps_display = Some(entity);
        }
      });
    }

    let mut ui_text = state.world.write_storage::<UiText>();
    if let Some(fps_display) = self.fps_display.and_then(|entity| ui_text.get_mut(entity)) {
      if state.world.read_resource::<Time>().frame_number() % 20 == 0 {
        let fps = state.world.read_resource::<FPSCounter>().sampled_fps();
        fps_display.text = format!("FPS: {:.2}", fps);
      }
    }

    Trans::None
  }

  fn handle_event(&mut self, _: StateData<GameData>, event: StateEvent) -> SimpleTrans {
    match &event {
      StateEvent::Window(e) => {
        if is_close_requested(&e) || is_key_down(&e, VirtualKeyCode::Escape) {
          Trans::Quit
        } else {
          Trans::None
        }
      }
      _ => Trans::None,
    }
  }
}

fn init_camera(world: &mut World) {
  let mut trans = Transform::default();
  trans.translate_xyz(0., 0., 1.);
  world
    .create_entity()
    .with(trans)
    .with(Camera::standard_2d())
    .with(CameraOrtho::normalized(CameraNormalizeMode::Contain))
    .build();
}

fn init_background(world: &mut World) {
  let texture = {
    let texture_res = world.read_resource::<TextureResource>();
    texture_res
      .get("background")
      .expect("Can't find background sprite")
      .clone()
  };
  let mut trans = Transform::default();

  // TODO: find out how to get cam and texture size
  trans.set_scale(1. / 1024., 1. / 1024., 0.);

  world.create_entity().with(texture).with(trans).build();
}

fn init_player(world: &mut World) {
  let mut trans = Transform::default();
  trans.set_scale(1. / 256., 1. / 256., 0.);

  let sprite_sheet = {
    let sprite_sheet_res = world.read_resource::<SpriteSheetResource>();
    sprite_sheet_res
      .get("player")
      .expect("Can't find player sprite")
      .clone()
  };

  let sprite_render = SpriteRender {
    sprite_sheet: sprite_sheet,
    sprite_number: 27,
  };

  let entity = world
    .create_entity()
    .with(sprite_render)
    .with(trans)
    .with(Player {})
    .with(Transparent)
    .build();

  let mut anim_control_storage = world.write_storage();
  let anim_set =
    get_animation_set::<Animations, SpriteRender>(&mut anim_control_storage, entity).unwrap();

  let anim_res = world.read_resource::<AnimationResource>();
  let anim = anim_res.get(&Animations::Wait).unwrap();
  let anim_control = AnimationControl::new(
    anim.clone(),
    EndControl::Loop(None),
    ControlState::Requested,
    AnimationCommand::Start,
    10.,
  );
  anim_set.insert(Animations::Wait, anim_control);
}

fn init_zombies(world: &mut World) {
  [(0.5, 0.5), (0.5, -0.5), (-0.5, 0.5), (-0.5, -0.5)]
    .iter()
    .for_each(|(x, y)| {
      let mut trans = Transform::default();
      trans.set_scale(1. / 256., 1. / 256., 0.);
      trans.set_xyz(*x, *y, 0.0);

      let sprite_sheet = {
        let sprite_sheet_res = world.read_resource::<SpriteSheetResource>();
        sprite_sheet_res
          .get("zombie")
          .expect("Can't find zombie sprite")
          .clone()
      };

      let sprite_render = SpriteRender {
        sprite_sheet: sprite_sheet,
        sprite_number: 27,
      };

      let entity = world
        .create_entity()
        .with(sprite_render)
        .with(trans)
        .with(Zombie {})
        .with(Transparent)
        .build();

      let mut anim_control_storage = world.write_storage();
      let anim_set =
        get_animation_set::<Animations, SpriteRender>(&mut anim_control_storage, entity).unwrap();

      let anim_res = world.read_resource::<AnimationResource>();
      let anim = anim_res.get(&Animations::Wait).unwrap();
      let anim_control = AnimationControl::new(
        anim.clone(),
        EndControl::Loop(None),
        ControlState::Requested,
        AnimationCommand::Start,
        10.,
      );
      anim_set.insert(Animations::Wait, anim_control);
    });
}
